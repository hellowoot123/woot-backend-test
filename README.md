# Woot-test

#Installation

```shell script
git clone https://gitlab.com/hellowoot123/woot-backend-test.git
```

## Do test                                                        
1. runserver 하여 웹브라우저에서 local 서버 접속                   
2. /user, /post url 접속하여 데이터 테이블 확인
3. 코드 수정하여 빈칸 채우기                                       
	- user/ `post_count`, `total_bookmark_count`
	- post/ `max_score`, `average_score`                           
4. test/<your name> branch 생성
5. commit (commit message 상세히 작성)                             
6. push!
