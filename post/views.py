from django.views.generic import ListView

from account.models import User
from post.models import Post


class UserListView(ListView):
    model = User
    context_object_name = 'users'
    template_name = 'account.html'

    def get_queryset(self):
        queryset = self.model.objects.all()
        # Write your code below
        return queryset


class PostListView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'post.html'

    def get_queryset(self):
        queryset = self.model.objects.all()
        # Write your code below
        return queryset
