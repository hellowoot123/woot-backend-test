from django.db import models


class Post(models.Model):
    user = models.ForeignKey('account.User', related_name='posts', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, help_text='제목 태그')
    content = models.TextField(default='', help_text='게시물 내용')
    created = models.DateTimeField(auto_now_add=True, db_index=True, help_text='생성 시점')
    users_like = models.ManyToManyField('account.User', related_name='posts_liked', blank=True, help_text='좋아요 한 유저들')
    users_bookmark = models.ManyToManyField('account.User', through='PostBookMark', related_name='posts_set_bookmark',
                                            blank=True,
                                            help_text='북마크')


class PostBookMark(models.Model):
    user = models.ForeignKey('account.User', related_name='post_bookmarks', on_delete=models.CASCADE, help_text='유저')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='bookmarks', help_text='포스트')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('user', 'post'),)


class PostScore(models.Model):
    user = models.ForeignKey('account.User', related_name='post_scores', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='scores', on_delete=models.CASCADE)
    score = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = (('user', 'post', ),)
