from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    first_name = models.CharField(max_length=30, blank=True, help_text='미사용')
    last_name = models.CharField(max_length=150, blank=True, help_text='미사용')
    email = models.EmailField(blank=True, help_text='미사용')

    objects = UserManager()

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self._nick()

    class Meta:
        db_table = 'auth_user'

    def _nick(self):
        try:
            if str(self.profile):
                return str(self.profile)
            return self.username + "(no nickname)"
        except:
            return self.username + "(no profile)"

    def get_full_name(self):
        pass

    def get_short_name(self):
        pass
